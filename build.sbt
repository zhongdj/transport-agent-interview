import org.scalastyle.sbt.ScalastylePlugin.autoImport._

name := "transport-agent-interview"

version := "1.0"

scalaVersion := "2.12.2"

lazy val akkaVersion = "2.5.3"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test",
  "org.mockito" % "mockito-core" % "2.15.0" % "test",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test"
)
parallelExecution in Test := false
coverageEnabled := true
coverageExcludedFiles := ".*Main"
coverageHighlighting := true
coverageMinimum := 80
coverageFailOnMinimum := true

lazy val compileScalastyle = taskKey[Unit]("compileScalastyle")
compileScalastyle := scalastyle.in(Compile).toTask("").value
scalastyleFailOnWarning := true
(compile in Compile) := ((compile in Compile) dependsOn compileScalastyle).value
