package gov.lbl.beam.interview.tasks.fourth

import akka.actor.{Actor, ActorSystem, PoisonPill, Props, Terminated}
import akka.testkit.{ImplicitSender, TestFSMRef, TestKit}
import com.typesafe.config.{Config, ConfigFactory}
import gov.lbl.beam.interview.tasks.fourth.SchedulerFSM.{Acknowledge, Initialized, Running, Start, Trigger}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.duration._
import scala.util.Random

object SchedulerSpec {
  val testConf: Config = ConfigFactory.parseString(
    """akka {
    loggers = ["akka.testkit.TestEventListener"]
    loglevel = "DEBUG"
    stdout-loglevel = "DEBUG"
    actor {
      debug {
        # enable function of LoggingReceive, which is to log any received message at
        # DEBUG level
        receive = on
        lifecycle = on
      }
      default-dispatcher {
        executor = "fork-join-executor"
        fork-join-executor {
          parallelism-min = 8
          parallelism-factor = 2.0
          parallelism-max = 8
        }
      }
    }
  }""")

}

class SchedulerSpec extends TestKit(ActorSystem("SchedulerSpec", SchedulerSpec.testConf))
  with WordSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender {
  val timeout_500_millis = 500 millis

  "Scheduler" must {

    "be initialized initial Trigger Messages for each agent" in {
      //Given
      val agentNumber = 5
      val dummyAgentSet = 1 to agentNumber map (id => system.actorOf(Props(new DummyAgent(id)))) toSet
      //When
      val fsm = TestFSMRef(new SchedulerFSM(dummyAgentSet))
      //Then
      fsm.stateName should be(Initialized)
      fsm.stateData.isEmpty should be(false)
      fsm.stateData.size should be(agentNumber)
      fsm.stateData.tasks.foreach { theTask =>
        dummyAgentSet.contains(theTask.agent) should be(true)
        theTask.trigger.time should be > 0L
      }
    }

    "while receives a Start event, it take a task from queue and send it to corresponding agent" in {
      //Given
      val agentNumber = 5
      val dummyAgentSet = 1 to agentNumber map (id => system.actorOf(Props(new DummyAgent(id)))) toSet
      val fsm = TestFSMRef(new SchedulerFSM(dummyAgentSet))
      //When
      fsm ! Start
      //Then
      expectMsg(timeout_500_millis, TriggerReceived)
      fsm.stateName should be(Running)
      fsm.stateData.size should be(agentNumber - 1)
    }

    "store a task after it receives Trigger message from a known agent" in {
      //Given
      val fsm = TestFSMRef(new SchedulerFSM(Set(testActor)))
      fsm ! Start
      val receivedTime = expectMsgType[Trigger].time
      //When
      fsm ! Trigger(receivedTime + Random.nextInt(100).abs)
      //Then
      fsm.stateName should be(Running)
      fsm.stateData.size should be(1)
      fsm.stateData.tasks.head.agent should be(testActor)
    }

    "stays at Running state while receives Acknowledge if task queue is empty" in {
      //Given
      val fsm = TestFSMRef(new SchedulerFSM(Set(testActor)))
      fsm ! Start
      val receivedTime = expectMsgType[Trigger].time
      //When
      fsm ! Trigger(receivedTime + Random.nextInt(100).abs)
      fsm ! Acknowledge
      expectMsgType[Trigger]
      fsm ! Acknowledge
      fsm ! Acknowledge
      expectNoMsg
      //Then
      fsm.stateData.isEmpty should be(true)
      fsm.stateName should be(Running)
    }

    "send a task to agent while task queue is not empty" in {
      //Given
      val fsm = TestFSMRef(new SchedulerFSM(Set(testActor)))
      fsm ! Start
      val receivedTime = expectMsgType[Trigger].time
      fsm ! Trigger(receivedTime + 10)
      //When
      fsm ! Acknowledge
      expectMsg(Trigger(receivedTime + 10))
      fsm.stateData.isEmpty should be(true)
    }

    "stop itself until all agents terminated" in {
      val agentNumber = 3
      val dummyAgentSet = 1 to agentNumber map (id => system.actorOf(Props(new DummyAgent(id)))) toSet
      val fsm = TestFSMRef(new SchedulerFSM(dummyAgentSet))
      watch(fsm)
      fsm.stateData.size should be(3)
      fsm ! Start
      expectMsg(200 millis, TriggerReceived)
      fsm.stateData.size should be(2)
      fsm.tell(Acknowledge, dummyAgentSet.head)
      expectMsg(200 millis, TriggerReceived)
      fsm.stateData.size should be(1)
      fsm.tell(Acknowledge, dummyAgentSet.head)
      expectMsg(200 millis, TriggerReceived)
      fsm.stateData.isEmpty should be(true)
      //When
      dummyAgentSet foreach (_ ! PoisonPill)
      //Then
      expectMsgType[Terminated](200 millis).actor should be(fsm)
    }
  }

  case object TriggerReceived

  class DummyAgent(id: Int) extends Actor {
    override def receive: Receive = {
      case Trigger(time) => testActor ! TriggerReceived
    }
  }

}
