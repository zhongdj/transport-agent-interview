package gov.lbl.beam.interview.tasks.common

import akka.actor.{Actor, ActorIdentity, ActorRef, FSM, Identify, Props}
import akka.pattern.ask
import akka.testkit.{TestFSMRef, TestKit}
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.reflect.ClassTag

trait Spyable {

  self: TestKit =>
  implicit val iTimeout = Timeout(5 seconds)
  implicit val executionContext = system.dispatcher

  def entrySpyBarrier(agent: ActorRef) = {
    Await.result(identify(agent), 5 seconds)
  }

  private def identify(agent: ActorRef): Future[ActorIdentity] = {
    agent ? Identify(None) mapTo (ClassTag(classOf[ActorIdentity])) recoverWith { case _ => identify(agent) }
  }

  def spyActor[T <: Actor](creator: => T)(implicit tag: ClassTag[T]): (T, ActorRef) = {
    import org.mockito.Mockito._
    lazy val spied = spy[T](creator)
    val actorRef = system.actorOf(Props(spied))
    entrySpyBarrier(actorRef)
    (spied, actorRef)
  }

  def spyFSMDeprecated[T <: FSM[_, _]](creator: => T)(implicit tag: ClassTag[T]): T = {
    /*
    import org.mockito.Mockito._
    lazy val spied = spy[T](creator)
    val actorRef = system.actorOf(Props(spied))
    entrySpyBarrier(actorRef)
    spied
    */
    creator
  }

  def spyFSM[S, D, T <: Actor](creator: => T)(implicit tag: ClassTag[T], ev: T <:< FSM[S, D]): (T, TestFSMRef[S, D, T]) = {
    import org.mockito.Mockito._
    lazy val spied = spy[T](creator)
    val testFSMRef = TestFSMRef(spied)
    entrySpyBarrier(testFSMRef)
    (spied, testFSMRef)
  }
}
