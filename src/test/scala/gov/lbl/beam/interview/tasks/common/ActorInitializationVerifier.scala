package gov.lbl.beam.interview.tasks.common

import akka.actor.SupervisorStrategy.Stop
import akka.actor.{Actor, ActorInitializationException, OneForOneStrategy, Props, SupervisorStrategy}

class ActorInitializationVerifier(props: => Props) extends Actor {

  val program = context.actorOf(props)

  override def receive = {
    case _ =>
  }

  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy() {
    case e: ActorInitializationException => stopChild
  }

  def stopChild: SupervisorStrategy.Directive = Stop

}
