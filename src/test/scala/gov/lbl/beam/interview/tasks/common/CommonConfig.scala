package gov.lbl.beam.interview.tasks.common

import com.typesafe.config.ConfigFactory

object CommonConfig {
  val config = ConfigFactory.parseString(
    """
      |akka.loggers = [akka.event.slf4j.Slf4jLogger]
      |akka.loglevel = debug
      |akka {
      |  actor {
      |    debug {
      |      # enable function of LoggingReceive, which is to log any received message at
      |      # DEBUG level
      |      receive = on
      |      lifecycle = on
      |    }
      |  }
      |}
    """.stripMargin)
}
