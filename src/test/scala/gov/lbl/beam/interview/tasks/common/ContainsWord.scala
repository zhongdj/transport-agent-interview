package gov.lbl.beam.interview.tasks.common

import org.scalatest.matchers.{MatchResult, Matcher}

final class ContainsWord {
  def apply(right: String): Matcher[String] = {
    new Matcher[String] {
      def apply(left: String): MatchResult = {
        val failureMessage = s"\n${left} \nshould contains \n${right}"
        MatchResult(
          left.contains(right),
          failureMessage,
          failureMessage,
          Vector(left)
        )
      }
    }
  }

}
