package gov.lbl.beam.interview.tasks.first

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import gov.lbl.beam.interview.tasks.common.{ActorInitializationVerifier, CommonConfig, Spyable}
import gov.lbl.beam.interview.tasks.first.Agent.AgentEvent
import gov.lbl.beam.interview.tasks.first.Program.Start
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpecLike}

import scala.concurrent.duration._

class TaskOneSpec extends TestKit(ActorSystem("task1-spec", CommonConfig.config))
  with WordSpecLike
  with MustMatchers
  with ImplicitSender
  with BeforeAndAfterAll with Spyable {

  class ProgramSpy(n: Int) extends Program(n) {
    override def createAgent(id: Int, next: Option[ActorRef]): ActorRef = testActor
  }


  "main program" must {

    "does not accept agentNumber as 0" in {
      //Given When
      val (spied, ref) = spyActor(new ActorInitializationVerifier(Props(new Program(0))))
      //Then
      verify(spied, timeout(100)).stopChild
    }

    "send message to actor id=1 with numberOfHopsTravelled initialized as 0" in {
      //Given
      val program = system.actorOf(Props(new ProgramSpy(1)))
      //When
      program ! Start
      //Then
      expectMsg(AgentEvent(0))
    }

    "not send message to actor id=2" in {
      //Given
      val program = system.actorOf(Props(new ProgramSpy(2)))
      //When
      program ! Start
      //Then
      expectMsg(AgentEvent(0))
      expectNoMsg(3 seconds)
    }
  }

  "Agent" must {

    "increment received message's numberOfHopsTravelled, and pass the info to the next neighbor if exists" in {
      //Given
      val agent = system.actorOf(Props(new Agent(1, Some(testActor))))
      //When
      agent ! AgentEvent(0)
      //Then
      expectMsg(AgentEvent(1))
    }

    "if it is the last agent, do not increment numberOfHopsTravelled, and just log message" in {
      //Given
      val (theSpy, agent) = spyActor[Agent](new Agent(1, None))
      //When
      agent ! AgentEvent(0)
      //Then
      verify(theSpy, timeout(500)).logOnly(s"Agent id=1 received numberOfHopsTravelled=0")
    }
  }

}
