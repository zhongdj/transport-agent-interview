package gov.lbl.beam.interview.tasks.fourth

import akka.actor.{ActorSystem, Terminated}
import akka.testkit._
import com.typesafe.config.{Config, ConfigFactory}
import gov.lbl.beam.interview.tasks.common.Spyable
import gov.lbl.beam.interview.tasks.fourth.AgentFSM.{Active, Passivate}
import gov.lbl.beam.interview.tasks.fourth.SchedulerFSM.{Acknowledge, Trigger}
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.reflect.ClassTag

object AgentSpec {
  val testConf: Config = ConfigFactory.parseString(
    """akka {
    loggers = ["akka.testkit.TestEventListener"]
    loglevel = "DEBUG"
    stdout-loglevel = "DEBUG"
    actor {
      debug {
        # enable function of LoggingReceive, which is to log any received message at
        # DEBUG level
        receive = on
        lifecycle = on
      }
      default-dispatcher {
        executor = "fork-join-executor"
        fork-join-executor {
          parallelism-min = 8
          parallelism-factor = 2.0
          parallelism-max = 8
        }
      }
    }
  }""")

  def loggerWrapper: LoggerWrapper = when(mock(classOf[LoggerWrapper]).print(anyString())).then(new Answer[Unit] {
    override def answer(invocation: InvocationOnMock): Unit = {
      print(invocation.getArgument[String](0))
    }
  }).getMock[LoggerWrapper]

}

class AgentSpec extends TestKit(ActorSystem("AgentSpec", AgentSpec.testConf))
  with WordSpecLike with BeforeAndAfterAll with BeforeAndAfter with Matchers with ImplicitSender with Spyable {
  val inMultiThreading = 200

  import AgentSpec._

  "agent" must {
    "be initialized as Active state" in {
      //Given
      val agentId = 1
      implicit var logger: LoggerWrapper = loggerWrapper
      //When
      val fsm = TestFSMRef(spyFSMDeprecated(new AgentFSM(agentId)))
      //Then
      fsm.stateName should be(Active)
    }

    "reply both Trigger and Acknowledge messages in Active state" in {
      //Given
      val agentId = 1
      val triggerTime = 100L
      implicit var logger: LoggerWrapper = loggerWrapper
      val (spied, fsm) = spyFSM(new AgentFSM(agentId))
      //When
      fsm ! Trigger(triggerTime)

      //Then
      entrySpyBarrier(fsm)
      verify(logger, timeout(inMultiThreading)).print(anyString)
      expectMsgType[Trigger].time should be >= triggerTime
      expectMsg(Acknowledge)
    }

    """passivate itself after receive 9 Trigger Messages,
      |and reply Acknowledge message only on 10th Trigger message, and then Terminated""".stripMargin in {
      //Given
      val agentId = 1
      val triggerTime = 100L
      implicit var logger: LoggerWrapper = loggerWrapper
      val (spied, fsm) = spyFSM(new AgentFSM(agentId))
      //When
      val triggeredCount = 9
      1 to triggeredCount foreach (_ => fsm ! Trigger(triggerTime))
      verify(logger, times(triggeredCount)).print(anyString())

      fsm.stateName should be(Passivate)
      fsm.stateData.triggeredCount should be(triggeredCount)
      1 to triggeredCount foreach { _ =>
        expectMsgType[Trigger]
        expectMsg(Acknowledge)
      }
      watch(fsm)
      fsm ! Trigger(triggerTime)
      expectMsg(Acknowledge)
      expectMsgType[Terminated].actor should be(fsm)
    }

  }

}
