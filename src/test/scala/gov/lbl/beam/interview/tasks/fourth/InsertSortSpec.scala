package gov.lbl.beam.interview.tasks.fourth

import org.scalatest.{Matchers, WordSpecLike}

class InsertSortSpec extends WordSpecLike with Matchers {

  "Insert Sort" must {
    "sort (10, List(1, 2, 11, 20)) as List(1, 2, 10, 11, 20)" in {
      new InsertSort[Int] {}.insertSort(List(1, 2, 11, 20), 10) should be(List(1, 2, 10, 11, 20))
    }

    "sort (10, List(11, 20)) as List(10, 11, 20)" in {
      new InsertSort[Int] {}.insertSort(List(11, 20), 10) should be(List(10, 11, 20))
    }

    "sort (10, List(11, 12, 20)) as List(10, 11, 12, 20)" in {
      new InsertSort[Int] {}.insertSort(List(11, 12, 20), 10) should be(List(10, 11, 12, 20))
    }

    "sort (20, List(10)) as List(10, 20)" in {
      new InsertSort[Int] {}.insertSort(List(10), 20) should be(List(10, 20))
    }
  }

}
