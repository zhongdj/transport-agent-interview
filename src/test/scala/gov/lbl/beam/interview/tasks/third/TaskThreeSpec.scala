package gov.lbl.beam.interview.tasks.third

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.pattern.pipe
import akka.testkit.{ImplicitSender, TestKit}
import akka.util.Timeout
import gov.lbl.beam.interview.tasks.common.{ActorInitializationVerifier, CommonConfig, Spyable}
import gov.lbl.beam.interview.tasks.third.ForwardingAgent.{Accepted, Join}
import gov.lbl.beam.interview.tasks.third.Program.{Initialize, Initialized, Start}
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.duration._
import scala.reflect.ClassTag

class TaskThreeSpec extends TestKit(ActorSystem("TaskTwo", CommonConfig.config))
  with WordSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender with Spyable {

  val timeout_500_millis = 500 millis

  "That randomly picked initial contact point actor " must {
    " send the message to its two neighbors" in {
      //Given
      val neighborOne = system.actorOf(Props(new DummyNeighbor(1)))
      val neighborTwo = system.actorOf(Props(new DummyNeighbor(2)))
      val agentId = 5
      val agent = system.actorOf(Props(new ForwardingAgent(agentId, neighborOne)))
      join(neighborTwo, agent)
      val helloMessage = s"hello from: ${agentId}"
      //When
      agent ! helloMessage

      expectMsgAllOf(timeout_500_millis, MessageReceivedOn(1, helloMessage), MessageReceivedOn(2, helloMessage))
    }
  }

  private def join(neighborTwo: ActorRef, agent: ActorRef) = {
    neighborTwo ! Register(agent)
    expectMsg(Accepted)
  }

  "Each neighbor receiving the message  " must {
    "forward it to that neighbor which did not send the message to it, after introducing a random delay between 1 and 100ms" in {
      val neighborOne = system.actorOf(Props(new DummyNeighbor(1)))
      val agentId = 6
      val (spied, agent) = spyActor(new ForwardingAgent(agentId, neighborOne))
      join(agent)
      val helloMessage = s"hello from: ${agentId}"

      //When
      agent ! helloMessage

      //Then
      expectMsg(timeout_500_millis, MessageReceivedOn(1, helloMessage))
    }
  }

  private def join(agent: ActorRef) = {
    agent ! Join(testActor)
    expectMsg(Accepted)
  }

  "When an actor has received a message from both of its neighbors, it " must {
    "print out its own Id and the message it received." in {
      //Given
      val neighborOne = system.actorOf(Props(new DummyNeighbor(1)))
      val neighborTwo = system.actorOf(Props(new DummyNeighbor(2)))
      val agentId = 5
      val (spied, agent) = spyActor(new ForwardingAgent(agentId, neighborOne))
      join(neighborTwo, agent)
      val helloMessage = s"hello from: 10"
      when(spied.randomDelay).thenReturn(200 milliseconds)

      //When
      neighborOne ! ForwardingMessage(agent, helloMessage)
      neighborTwo ! ForwardingMessage(agent, helloMessage)

      //Then
      expectNoMsg(timeout_500_millis)
    }
  }

  "When an actor has received a message from the last neighbor " must {
    " print out its own Id and the message it received." in {
      //Given
      val neighborOne = system.actorOf(Props(new DummyNeighbor(1)))
      val neighborTwo = system.actorOf(Props(new DummyNeighbor(2)))
      val agentId = 5
      val (spied, agent) = spyActor(new ForwardingAgent(agentId, neighborOne))
      join(neighborTwo, agent)
      val helloMessage = s"hello from: 10"
      when(spied.randomDelay).thenReturn(200 milliseconds)

      //When
      neighborOne ! ForwardingMessage(agent, helloMessage)
      neighborTwo ! ForwardingMessage(agent, helloMessage)

      //Then
      expectNoMsg(timeout_500_millis)
    }
  }

  "main program" must {
    val inMultiThreading = 100
    "not accept agentNumber as 2" in {
      //Given When
      val (spied, ref) = spyActor(new ActorInitializationVerifier(Props(new Program(2))))
      //Then
      verify(spied, timeout(inMultiThreading)).stopChild
    }

    "create 3 Agent while agentNumber=3" in {
      //Given
      val (spied, ref) = spyActor(new Program(3))

      //When
      ref ! Initialize
      expectMsg(Initialized)
      //Then
      verify(spied, timeout(inMultiThreading).times(3))
        .createAgent(anyInt(), any[Option[ActorRef]]())

      expectNoMsg(timeout_500_millis)
    }

    "send message to a specified agent" in {
      //Given
      val (spied, ref) = spyActor(new Program(3))
      when(spied.createAgent(3, None)).thenReturn(testActor)
      //When
      ref ! Initialize
      expectMsgType[Join](timeout_500_millis)
      lastSender ! Accepted
      expectMsgType[Join](timeout_500_millis)
      lastSender ! Accepted
      expectMsg(timeout_500_millis, Initialized)

      //Then
      ref ! Start(1)
      expectMsg(timeout_500_millis, "hello from: 1") // from 1
      expectMsg(timeout_500_millis, "hello from: 1") // from 2
      expectNoMsg(timeout_500_millis)
    }
  }

  case class Register(recipient: ActorRef)

  case class MessageReceivedOn(i: Int, theMessage: String)

  case class ForwardingMessage(recipient: ActorRef, theMessage: String)

  class DummyNeighbor(id: Int)(implicit tag: ClassTag[Accepted.type]) extends Actor {
    implicit val iTimeout = Timeout(5 seconds)

    import akka.pattern.ask

    override def receive: Receive = {
      case helloMessage: String =>
        testActor ! MessageReceivedOn(id, helloMessage)
      case ForwardingMessage(recipientA, theMessage) =>
        recipientA ! theMessage
      case Register(recipient) =>
        recipient ? Join(self) mapTo (tag) pipeTo sender
    }
  }

}
