package gov.lbl.beam.interview.tasks.second

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import gov.lbl.beam.interview.tasks.common.{CommonConfig, Spyable}
import gov.lbl.beam.interview.tasks.common.MatchWordsExt._
import gov.lbl.beam.interview.tasks.first.Agent.{AgentMessage, EmptyMessage, Millisecond, NonEmptyMessages}
import gov.lbl.beam.interview.tasks.first.Agent
import org.mockito.ArgumentCaptor
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}


class TaskTwoSpec extends TestKit(ActorSystem("TaskTwo", CommonConfig.config))
  with FlatSpecLike with Matchers with BeforeAndAfterAll with ImplicitSender with Spyable {

  "initial, but not last agent" should "accumulate messages with Nil and its own message and pass them to next" in {
    //Given
    val (spied, agent) = spyActor(new Agent(1, Some(testActor)))
    val startTimestamp = System.currentTimeMillis()
    //When
    agent ! EmptyMessage
    //Then
    val agentOneSentMessage = expectMsgType[NonEmptyMessages]
    agentOneSentMessage.items.size should be(1)
    agentOneSentMessage.items.head.id should be(1)
    agentOneSentMessage.items.head.receivedMillis should be >= startTimestamp
  }

  "medium (not initial or not last) agent" should "accumulate messages with received messages and its own message and pass them to next" in {
    //Given
    val (spied, agent) = spyActor(new Agent(3, Some(testActor)))
    val startTimestamp: Millisecond = System.currentTimeMillis()
    //When
    agent ! NonEmptyMessages(AgentMessage(1, startTimestamp) :: AgentMessage(2, startTimestamp) :: Nil)
    //Then
    val agentOneSentMessage = expectMsgType[NonEmptyMessages]
    agentOneSentMessage.items.size should be(3)
    agentOneSentMessage.items(0).id should be(1)
    agentOneSentMessage.items(0).receivedMillis should be(startTimestamp)

    agentOneSentMessage.items(1).id should be(2)
    agentOneSentMessage.items(1).receivedMillis should be(startTimestamp)

    agentOneSentMessage.items(2).id should be(3)
    agentOneSentMessage.items(2).receivedMillis should be >= startTimestamp
  }

  "last agent" should "accumulate messages with received ones and its own message and print message only" in {
    val (spied, lastAgent) = spyActor(new Agent(4, None))
    val startTimestamp: Millisecond = 1518928843228L
    //When
    lastAgent ! NonEmptyMessages(AgentMessage(1, startTimestamp) :: AgentMessage(2, startTimestamp) :: AgentMessage(3, startTimestamp) :: Nil)
    //Then
    import org.mockito.Mockito._
    val logMessageCaptor = ArgumentCaptor.forClass(classOf[String])
    verify(spied, timeout(100)).logOnly(logMessageCaptor.capture())
    val logMessage: String = logMessageCaptor.getValue()
    logMessage should contains("actor 1, message received <1518928843228>")
    logMessage should contains("actor 2, message received <1518928843228>")
    logMessage should contains("actor 3, message received <1518928843228>")
    logMessage should contains("actor 4, message received")
  }


}