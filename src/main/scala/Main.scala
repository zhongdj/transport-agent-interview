object Main extends App {

  sealed trait Segment {
    def matches(s: Segment): Boolean = (this, s) match {
      case (Root, Root) => true
      case (Method(m), Method(n)) if m == n => true
      case (Variable(m), Fixed(value)) =>
        println(s"${m} = ${value}")
        true
      case (Wildcard(m), Fixed(value)) =>
        println(s"${m} = ${value}")
        true
      case (Fixed(m), Fixed(n)) if m == n => true
      case _ => false
    }
  }

  case object Root extends Segment
  case class Method(name: String) extends Segment
  case class Fixed(value: String) extends Segment
  case class Variable(name: String) extends Segment
  case class Wildcard(name: String) extends Segment
  case class Handler(h: () => String) extends Segment

  val variable = """:(.*)""".r
  val wildcard = """\*(.*)""".r

  def parse: String => Segment = {
    case wildcard(name) => Wildcard(name)
    case variable(name) => Variable(name)
    case value => Fixed(value)
  }

  def path(method: String, url: String): Path =
    url.split("/").filter(_.length > 0).map(parse).foldLeft[List[Segment]](List(Method(method), Root)) {
      case (acc@(Wildcard(name) :: _), _) => acc
      case (acc, x) => x :: acc
    }.reverse

  type Path = List[Segment]

  trait Tree[+T] {
    import Tree._
    def +[S >: T](other: Tree[S]): Tree[S] = (this, other) match {
      case (_, Empty) => this
      case (Empty, _) => other
      case (Node(x, xs), Node(y, ys)) if (x == y) => Node[S](x, ++(xs, ys))
    }
  }

  object Tree {
    private def ++[T](xs: List[Tree[T]], ys: List[Tree[T]]) = {
      val (mergable, unmergable) = (xs ::: ys)
        .filter(_ != Empty)
        .map(_.asInstanceOf[Node[T]])
        .groupBy(_.label)
        .partition(_._2.size > 1)
      val merged = mergable.values
        .map(xs => xs.reduce(_.+(_).asInstanceOf[Node[T]]))
        .toList
      merged ::: unmergable.values.flatten.toList
    }
  }
  case class Node[T](label: T, children: List[Tree[T]]) extends Tree[T]
  case object Empty extends Tree[Nothing]

  def toTree(path: Path): Tree[Segment] = path.foldRight[Tree[Segment]](Empty) { (segment, node) => Node(segment, List(node)) }

  case class Router(method: String, url: String, handler: () => String)

  case class RouterBuilder(routers: List[Router] = Nil) {
    def get(url: String, handler: () => String): RouterBuilder = add("GET", url, handler)
    def put(url: String, handler: () => String): RouterBuilder = add("PUT", url, handler)
    def delete(url: String, handler: () => String): RouterBuilder = add("DELETE", url, handler)
    def post(url: String, handler: () => String): RouterBuilder = add("POST", url, handler)
    def build(): URLRouter = URLRouter(
      routers.map(r => path(r.method, r.url) ::: Handler(r.handler) :: Nil)
        .map(toTree)
        .reduce(_ + _)
    )
    private def add(method: String, url: String, handler: () => String) = {
      this.copy(routers = Router(method, url, handler) :: routers)
    }
  }

  def locate(tree: Tree[Segment], s: Segment): Option[Tree[Segment]] = tree match {
    case Empty => None
    case Node(_, children) => children.find {
      case n@Node(x, xs) if x.matches(s) => true
      case _ => false
    }
  }

  case class URLRouter(tree: Tree[Segment]) {
    def on(method: String, url: String): Option[String] = {
      val next = (path(method, url).tail).foldLeft(Option(tree))((nodeOpt, s) => nodeOpt.flatMap(locate(_, s)))
      next
        .map(_.asInstanceOf[Node[Segment]])
        .map(_.children)
        .flatMap {
          case Node(Handler(h), _) :: Nil => Some(h())
          case _ => None
        }
    }
  }

  println(
    RouterBuilder()
      .get("/todo/:id/something", () => "get todo")
      .post("/todo/:id/something", () => "post todo")
      .get("/done/:id/something", () => "done")
      .put("/ongoing/:id/something", () => "ongoing")
      .get("/todo/my/*status", () => "my status")
      .build()
      .on("GET", "/todo/456/something")
      .getOrElse("not found")
  )

}
