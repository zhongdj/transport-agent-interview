package gov.lbl.beam.interview.tasks.first

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.event.LoggingReceive
import gov.lbl.beam.interview.tasks.first.Agent.{AgentEvent, AgentMessage, TaskTwoMessage}

object Agent {

  case class AgentEvent(numberOfHopsTravelled: Int)

  type Millisecond = Long

  trait TaskTwoMessage {
    def cons(agentMsg: AgentMessage): TaskTwoMessage = NonEmptyMessages(items ++ (agentMsg :: Nil))

    def items: List[AgentMessage]
  }

  object EmptyMessage extends TaskTwoMessage {
    override def items: List[AgentMessage] = Nil
  }

  case class NonEmptyMessages(items: List[AgentMessage]) extends TaskTwoMessage

  case class AgentMessage(id: AgentId, receivedMillis: Millisecond)

}

class Agent(id: AgentId, next: Option[ActorRef]) extends Actor with ActorLogging {
  override def receive: Receive = LoggingReceive {
    case AgentEvent(numberOfHopsTravelled) => tellNeighborOrLog(
      taskOneNeighborMessage(numberOfHopsTravelled),
      taskOneLogFormat(numberOfHopsTravelled)
    )
    case receivedMsg: TaskTwoMessage => tellNeighborOrLog(
      createTaskTwoMsgBy(receivedMsg),
      taskTwoLogFormat(createTaskTwoMsgBy(receivedMsg))
    )
  }

  private def createTaskTwoMsgBy(taskTwoMessage: TaskTwoMessage) = {
    taskTwoMessage.cons(AgentMessage(id, System.currentTimeMillis()))
  }

  private def taskOneNeighborMessage(numberOfHopsTravelled: AgentId) = {
    AgentEvent(numberOfHopsTravelled + 1)
  }

  private def taskTwoLogFormat(m: TaskTwoMessage) = {
    m.items.map(agentMessage =>
      s"actor ${agentMessage.id}, message received <${agentMessage.receivedMillis}>"
    ).mkString("\n")
  }

  private def tellNeighborOrLog(aMessage: Any, logMessage: String) = {
    next
      .map[() => Unit](tellNeighbor(aMessage))
      .getOrElse(logOnly(logMessage))
      .apply()
  }

  def logOnly(logMessage: String): () => Unit = {
    () =>
      log.info(logMessage)
      context.system.terminate()
  }

  private def taskOneLogFormat(numberOfHopsTravelled: AgentId) = {
    s"Agent id=${id} received numberOfHopsTravelled=${numberOfHopsTravelled}"
  }

  def tellNeighbor(aMessage: Any): ActorRef => () => Unit = { neighbor: ActorRef =>
    () => (neighbor ! aMessage)
  }

}
