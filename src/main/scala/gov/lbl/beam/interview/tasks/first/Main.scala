package gov.lbl.beam.interview.tasks.first

import akka.actor.{ActorSystem, Props}
import gov.lbl.beam.interview.tasks.first.Program.Start

// $COVERAGE-OFF$Disabling
object Main extends App {
  if (args.length <= 0 || args.length > 1) throw new IllegalArgumentException(
    """
      |[Usage] sbt "runMain gov.lbl.beam.interview.tasks.first.Main totalAgentNumber"
      |
      |[Example] sbt "runMain gov.lbl.beam.interview.tasks.first.Main 30"
    """.stripMargin)

  val system = ActorSystem("TaskSystem-1")
  val agentNumber = args(0).toInt
  val program = system.actorOf(Props(new Program(agentNumber)), "Task-1")
  program ! Start
}

// $COVERAGE-ON$
