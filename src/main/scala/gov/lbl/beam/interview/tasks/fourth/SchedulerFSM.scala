package gov.lbl.beam.interview.tasks.fourth

import akka.actor.{ActorRef, LoggingFSM, Terminated}
import gov.lbl.beam.interview.tasks.fourth.SchedulerFSM.{Acknowledge, Initialized, Running, Start, StateData, Task, TaskQueue, Trigger}

import scala.util.Random

object SchedulerFSM {

  sealed trait StateName

  object Initialized extends StateName

  object Running extends StateName

  object Stopped extends StateName

  case class Task(agent: ActorRef, trigger: Trigger) {
    def perform(implicit sender: ActorRef): Unit = agent ! trigger
  }

  implicit val taskOrdering = Ordering.fromLessThan[Task]((one, other) => one.trigger.time < other.trigger.time)

  type StateData = TaskQueue

  case class TaskQueue(tasks: List[Task]) {

    def size: Int = tasks.size

    def isEmpty: Boolean = tasks.isEmpty

  }

  trait ScheduleEvent

  case object Start extends ScheduleEvent

  case class Trigger(time: Long) extends ScheduleEvent

  case object Acknowledge extends ScheduleEvent


}

class SchedulerFSM(agents: Set[ActorRef])
  extends LoggingFSM[SchedulerFSM.StateName, SchedulerFSM.StateData] with InsertSort[Task] {
  agents.foreach(context.watch)

  var activeAgents: Set[ActorRef] = agents.toSet

  lazy val initialTasks: List[Task] = agents.map(agent =>
    Task(agent, Trigger(Random.nextLong().abs))
  ).toList.sorted

  def taskForEachAgent: SchedulerFSM.StateData = TaskQueue(initialTasks)

  startWith(SchedulerFSM.Initialized, taskForEachAgent)

  when(Initialized) {
    case Event(Start, currentStateData) =>
      currentStateData.tasks.head.perform
      goto(Running)
        .using(TaskQueue(currentStateData.tasks.tail))
  }

  when(Running) {
    case Event(trigger: Trigger, currentData) if knows(sender) =>
      stay().using(TaskQueue(sort(trigger, currentData)))
    case Event(Acknowledge, currentData) if knows(sender) =>
      if (currentData.isEmpty) stay()
      else {
        currentData.tasks.head.perform
        stay().using(TaskQueue(currentData.tasks.tail))
      }
    case Event(Terminated(agent), currentData) =>
      activeAgents -= agent
      if (!activeAgents.isEmpty) stay()
      else stop()
  }

  private def sort(trigger: Trigger, currentData: StateData) = {
    val tasks = currentData.tasks
    val task = Task(sender(), trigger)
    insertSort(tasks, task)
  }

  private def knows(agent: ActorRef) = agents.contains(agent)

}
