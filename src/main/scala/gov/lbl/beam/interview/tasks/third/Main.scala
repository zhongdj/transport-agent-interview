package gov.lbl.beam.interview.tasks.third

import akka.actor.{ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import gov.lbl.beam.interview.tasks.third.Program.{Initialize, Start}

import scala.concurrent.duration._
import scala.util.Random

object Main extends App {
  if (args.length <= 0 || args.length > 1) throw new IllegalArgumentException(
    """
      |[Usage] sbt "runMain gov.lbl.beam.interview.tasks.third.Main totalAgentNumber"
      |
      |[Example] sbt "runMain gov.lbl.beam.interview.tasks.third.Main 17"
    """.stripMargin)

  val config = ConfigFactory.parseString(
    """
      |akka.loggers = [akka.event.slf4j.Slf4jLogger]
      |akka.loglevel = debug
      |akka {
      |  actor {
      |    debug {
      |      # enable function of LoggingReceive, which is to log any received message at
      |      # DEBUG level
      |      receive = off
      |      lifecycle = off
      |    }
      |    default-dispatcher {
      |      type = Dispatcher
      |      executor = "thread-pool-executor"
      |      thread-pool-executor {
      |        fixed-pool-size = 8
      |      }
      |    }
      |  }
      |}""".stripMargin)

  val system = ActorSystem("Task-3", config)
  val maxAgentNumber = args(0).toInt
  val program = system.actorOf(Props(new Program(maxAgentNumber)))
  implicit val timeout = Timeout(5 minutes)
  implicit val execCtx = system.dispatcher
  program ? Initialize andThen { case _ =>
    program ! Start(Random.nextInt(maxAgentNumber).abs)
  }

}
