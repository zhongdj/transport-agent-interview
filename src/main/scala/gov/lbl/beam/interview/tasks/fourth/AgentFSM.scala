package gov.lbl.beam.interview.tasks.fourth

import akka.actor.LoggingFSM
import gov.lbl.beam.interview.tasks.fourth.AgentFSM.{Active, Passivate, StateData}
import gov.lbl.beam.interview.tasks.fourth.SchedulerFSM.{Acknowledge, Trigger}

import scala.util.Random

object AgentFSM {

  trait StateName

  case object Active extends StateName

  case object Passivate extends StateName

  case class StateData(triggeredCount: Int) {
    def increase: StateData = StateData(triggeredCount + 1)

    def reachMaxActivated: Boolean = triggeredCount >= maxTriggered - 2
  }

  val max = 100
  val maxTriggered = 10

}

class AgentFSM(id: Int)(implicit loggerWrapper: LoggerWrapper)
  extends LoggingFSM[AgentFSM.StateName, AgentFSM.StateData] {

  startWith(Active, StateData(0))

  when(Active) {
    case Event(Trigger(time), currentData) =>
      logTrigger(s"agent: ${id}, Trigger(${time})")
      if (!currentData.reachMaxActivated) {
        stay()
          .using(currentData.increase)
          .replying(Trigger(augmented(time)))
          .replying(Acknowledge)
      } else {
        goto(Passivate)
          .using(currentData.increase)
          .replying(Trigger(augmented(time)))
          .replying(Acknowledge)
      }
  }

  when(Passivate) {
    case Event(Trigger(time), currentData) =>
      logTrigger(s"agent: ${id}, Trigger(${time})")
      stop()
        .using(currentData.increase)
        .replying(Acknowledge)
  }

  def logTrigger(message: String): Unit = loggerWrapper.print(message)

  private def augmented(time: Long) = time + Random.nextInt(AgentFSM.max).abs
}
