package gov.lbl.beam.interview.tasks.fourth

trait InsertSort[T] {

  def insertSort(tasks: List[T], task: T)(implicit ordering: Ordering[T]): List[T] = {
    implicit val x: T = task
    insertToNil orElse insertToSingle orElse insertToMore apply (tasks)
  }

  private def insertToNil(implicit x: T, ordering: Ordering[T]): PartialFunction[List[T], List[T]] = {
    case Nil => x :: Nil
  }

  private def insertToSingle(implicit x: T, ordering: Ordering[T]): PartialFunction[List[T], List[T]] = {
    case head :: Nil =>
      if (ordering.gt(head, x)) x :: head :: Nil
      else head :: x :: Nil
  }

  private def insertToMore(implicit x: T, ordering: Ordering[T]): PartialFunction[List[T], List[T]] = {
    case xs =>
      if (leftSide(x, xs)) {
        insertSort(xs.take(xs.size / 2), x) ::: xs.takeRight(xs.size - xs.size / 2)
      } else {
        xs.take(xs.size / 2) ::: insertSort(xs.takeRight(xs.size - xs.size / 2), x)
      }
  }

  private def leftSide(task: T, xs: List[T])(implicit ordering: Ordering[T]): Boolean = ordering.gt(xs(xs.size / 2), task)
}
