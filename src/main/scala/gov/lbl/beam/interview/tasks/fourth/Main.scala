package gov.lbl.beam.interview.tasks.fourth

import akka.actor.{ActorSystem, Props}
// $COVERAGE-OFF$
object Main extends App {

  if (args.length <= 0 || args.length > 1) throw new IllegalArgumentException(
    """
      |[Usage] sbt "runMain gov.lbl.beam.interview.tasks.fourth.Main totalAgentNumber"
      |
      |[Example] sbt "runMain gov.lbl.beam.interview.tasks.fourth.Main 17"|grep Trigger|wc -l
    """.stripMargin)

  val totalAgents = args(0).toInt
  val system = ActorSystem("Task-4")
  system.actorOf(Props(new Program(totalAgents)))

}
// $COVERAGE-ON$
