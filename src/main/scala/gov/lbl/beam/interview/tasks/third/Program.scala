package gov.lbl.beam.interview.tasks.third

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.ask
import akka.util.Timeout
import gov.lbl.beam.interview.tasks.first.AgentId
import gov.lbl.beam.interview.tasks.third.ForwardingAgent.{Accepted, Join}
import gov.lbl.beam.interview.tasks.third.Program.{Initialize, Initialized, Joined, Start}

import scala.concurrent.Future
import scala.concurrent.duration._

object Program {

  object Initialize

  object Initialized

  case class Start(i: Int)

  type Joined = Accepted.type
}

class Program(agentNumber: Int) extends Actor with ActorLogging {
  require(agentNumber > 2, s"agentNumber should greater than 2")
  var childrenMap = Map.empty[AgentId, ActorRef]
  implicit val timeout = Timeout(500 millis)
  implicit val execCtx = context.dispatcher

  override def receive: Receive = {
    case Initialize =>
      val theLast = makeAgent(agentNumber, None)
      val theHead = (1 until agentNumber).foldRight(theLast) { (id: AgentId, next: ActorRef) =>
        makeAgent(id, Some(next))
      }
      compensateHeadAndLast(theLast, theHead, linearConnect)
        .map(replyInitialized(sender()))
    case Start(i: Int) =>
      childrenMap(i) ! s"hello from: ${i}"
  }

  private def replyInitialized(client: ActorRef) = (allConnected: List[Joined]) => client ! Initialized

  private def linearConnect = (1 until agentNumber map toJoinerAndJoinee)
    .map { case (joiner, joinee) => performJoin(joiner, joinee) }
    .toList

  private def compensateHeadAndLast(theLast: ActorRef, theHead: ActorRef, futures: List[Future[Joined]]) = {
    Future.sequence(connectHeadAndLast(theLast, theHead) ::: futures)
  }

  private def connectHeadAndLast(theLast: ActorRef, theHead: ActorRef) = {
    performJoin(theHead, theLast) :: performJoin(theLast, theHead) :: Nil
  }

  private def performJoin(joiner: ActorRef, joinee: ActorRef) = joinee
    .ask(Join(joiner))(timeout, joiner)
    .mapTo[Joined]

  private def toJoinerAndJoinee(id: Int): (ActorRef, ActorRef) = (childrenMap(id), childrenMap(id + 1))

  private def makeAgent(id: Int, nextOp: Option[ActorRef]): ActorRef = {
    val child = createAgent(id, nextOp)
    childrenMap += (id -> child)
    child
  }

  def createAgent(id: AgentId, nextOp: Option[ActorRef]): ActorRef = {
    context.actorOf(Props(new ForwardingAgent(id, nextOp)))
  }
}
