package gov.lbl.beam.interview.tasks.fourth

import akka.actor.{Actor, ActorLogging, Props, Terminated}
import gov.lbl.beam.interview.tasks.fourth.SchedulerFSM.Start

// $COVERAGE-OFF$
class Program(n: Int) extends Actor with ActorLogging {

  implicit val loggerWrapper = new LoggerWrapper {
    override def print(message: String): Unit = log.info(message)
  }

  val agents = 1 to n map (id => context.actorOf(Props(new AgentFSM(id)))) toSet
  val scheduler = context.actorOf(Props(new SchedulerFSM(agents)))
  context.watch(scheduler)
  self ! "start"

  override def receive: Receive = {
    case "start" =>
      scheduler ! Start
    case Terminated(scheduler) =>
      context.stop(self)
      log.info("Program Finished.")
      context.system.terminate()
  }
}

// $COVERAGE-ON$
