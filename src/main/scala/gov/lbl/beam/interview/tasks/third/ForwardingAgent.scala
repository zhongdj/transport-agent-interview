package gov.lbl.beam.interview.tasks.third

import akka.actor.{Actor, ActorLogging, ActorRef, ReceiveTimeout}
import akka.event.LoggingReceive
import gov.lbl.beam.interview.tasks.first.AgentId
import gov.lbl.beam.interview.tasks.third.ForwardingAgent.{Accepted, Join}

import scala.concurrent.duration._
import scala.util.Random

object ForwardingAgent {

  case class Join(previousNeighbor: ActorRef)

  case object Accepted

}

class ForwardingAgent(id: AgentId, nextNeighbor: Option[ActorRef]) extends Actor with ActorLogging {
  def this(id: AgentId, nextNeighbor: ActorRef) = this(id, Some(nextNeighbor))

  var ignorantNeighbors: Set[ActorRef] = nextNeighbor.map(Set(_)).getOrElse(Set.empty)
  var neighbors: Set[ActorRef] = nextNeighbor.map(Set(_)).getOrElse(Set.empty)

  override def receive: Receive = receiving

  def receiving: Receive = LoggingReceive {
    neighborJoin orElse {
      case helloMessage: String =>
        ignorantNeighbors -= sender()
        //Cannot be Empty locally but can be Empty distributed
        if (ignorantNeighbors.isEmpty) {
// $COVERAGE-OFF$ it depends on message delivery strategy
          logMessage(helloMessage)
// $COVERAGE-ON$
        } else {
          context.become(sending(helloMessage))
          context.setReceiveTimeout(randomDelay)
        }
    }
  }

  def sending(helloMessage: String): Receive = LoggingReceive {
    neighborJoin orElse {
      case helloMessage: String =>
        ignorantNeighbors -= sender()
      case ReceiveTimeout =>
        context.setReceiveTimeout(Duration.Undefined)
        context.unbecome()
        if (ignorantNeighbors.isEmpty) {
          logMessage(helloMessage)
        } else {
          ignorantNeighbors.foreach(neighbor => neighbor ! helloMessage)
          //in a distributed env, this depends on message delivery strategy
          ignorantNeighbors = Set.empty
        }
    }
  }

  val maxDelay = 100
  def randomDelay: FiniteDuration = Random.nextInt(maxDelay).abs milliseconds

  def logMessage(helloMessage: String): Unit = {
    log.info(s"actor ${id}, received message: ${helloMessage}")
    //context.system.terminate()
  }

  private def neighborJoin: Receive = {
    case Join(previousNeighbor) =>
      ignorantNeighbors += previousNeighbor
      neighbors += previousNeighbor
      sender() ! Accepted
  }

}
