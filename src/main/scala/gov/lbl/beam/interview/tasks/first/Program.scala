package gov.lbl.beam.interview.tasks.first

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.event.LoggingReceive
import gov.lbl.beam.interview.tasks.first.Agent.AgentEvent
import gov.lbl.beam.interview.tasks.first.Program.Start

class Program(agentNumber: Int) extends Actor with ActorLogging {

  require(agentNumber > 0, s"agentNumber should greater than 0")


  def run : Unit = {
    val theLast = createAgent(agentNumber, None)
    val headAgent = (1 to agentNumber - 1).foldRight[ActorRef](theLast)(generateAgent)
    headAgent ! AgentEvent(0)
  }

  private def generateAgent(id: AgentId, next: ActorRef): ActorRef = {
    val theAgent = createAgent(id, Some(next))
    theAgent
  }

  protected def createAgent(id: Int, next: Option[ActorRef]): ActorRef = {
    // $COVERAGE-OFF$ while unit testing this method had been override
    context.actorOf(Props(new Agent(id, next)), s"agent-${id}")
    // $COVERAGE-ON$
  }

  override def receive: Receive = LoggingReceive {
    case Start => run
  }
}

object Program {

  object Start

}
