## Introduction

This program is an interview test for hiring Akka and Scala developer.

* [Design Ideas](./extras/DESIGN.md)

## Key Technique

### Essential Techniques
- JDK 8 +
- Scala 2.12.2
- SBT 0.13.15
- Akka 2.5.3

### Test Techniques
- Scalatest 3.0.1
- mockito 2.15.0
- Akka Testkit 2.5.3

### QA Techniques
- scalastyle-sbt-plugin 1.0.0
- sbt-scoverage 1.5.1
- sbt-codacy-coverage 1.3.11

### Key Coding Standard

- CyclomaticComplexityChecker <= 4
- Code Coverage Rate >= 80%

For further information, check following files:
```./scalastyle-config.xml and ./build.sbt``` 

## Code Report

### Main Code
![Coverage](./coverage.png)

### Test Code 
601 lines (by ```find . -name "*.scala"|xargs cat|grep -v ^$|wc -l```)

## Code Introduction

This project directory layout is created by the following command:

```sbt new akka/akka-scala-seed.g8```

The 4 tasks are implemented under: ```src/main/scala/gov/lbl/beam/interview/tasks```
and their corresponding test cases are under: ```src/test/scala/gov/lbl/beam/interview/tasks```

Task 1 and 2 are both implemented under ```gov.lbl.beam.interview.tasks.first``` package, 
but their test specs are implemented under ```gov.lbl.beam.interview.tasks.first``` and ```gov.lbl.beam.interview.tasks.second``` accordingly.  


## Running Program

### Run Task 1 & 2
```
sbt "runMain gov.lbl.beam.interview.tasks.first.Main 20"
```

### Run Task 3
```
sbt "runMain gov.lbl.beam.interview.tasks.third.Main 17
```
NOTE: this program does not terminate, no lifecycle watch strategy implemented

### Run Task 4 with 17 agents to check how many Trigger were logged by Agents
```
sbt "runMain gov.lbl.beam.interview.tasks.fourth.Main 17"|grep Trigger|wc -l
```

### Run large amount of agents
Before Running Task 4, execute following command:

```
export JAVA_TOOL_OPTIONS="-Xmx4G -Xms512m"
```

and then execute the following command:

```
sbt "runMain gov.lbl.beam.interview.tasks.fourth.Main 1000000"|grep Trigger|wc -l
```

NOTE: For Task 3, since there is random delay from 0 ~ 100 milliseconds per message forwarding, so large amount of agents will take a lot of time.

## Running Test Specs

```
sbt clean coverage test coverageReport
```

## Check Code Coverage Reports

Reports can be located from 
```
target/scala-2.12/scoverage-report/index.html
```

## Applied Development Practices
- Plant UML
  - gov/lbl/beam/interview/tasks/fourth/agent.fsm.puml
  - gov/lbl/beam/interview/tasks/fourth/scheduler.fsm.puml
- Test
  - BDD
  - TDD
  - Scalatest
  - Mockito
  - Akka TestKit
  - Customize
    - ActorInitializationVerifier
    - Integrate Mockito's Spy into Akka
- Scalastyle
  - scalastyle-sbt-plugin
  - Guide
     - already integrated with ```sbt compile```
- Code Coverage
  - sbt-scoverage
  - sbt-codacy-coverage
  - Guide
     - Generate Report with ```sbt clean coverage test coverageReport```
     - HTML report located at  ```target/scala-2.12/scoverage-report/index.html```


## Known Issues

 - [Task #4] While Mockito Spying FSM, FSM state's reply method cannot find sender. [Fixed]
 - [Task #4] While Mockito Spying FSM, verify(fsmSpy, times(x)).onMethod does not work. [Workaround with decoupling LoggerWrapper]
 - [Task #4] SchedulerFSM relies on a sort algorithm for sorting the queue, which leads to low efficiency when deal with large amount of agents. [Use Insert Sort Algorithm, to be verified]
 - [Task #4] Fork-and-join pool does not work efficiently for this task, fork-join-pool scan method take much of the time [Fixed by replacing with thread-pool-executor]
 - [Task #4] While using thread-pool-executor, the SchedulerFSM's Acknowledge-Trigger mechinism serialized all the activities between Scheduler and Agents. CPU cannot be utilized more than 1 core